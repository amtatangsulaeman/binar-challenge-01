import UIKit

/* Untuk meningkatkan skill teman-teman dalam mempelajari
 pengembangan aplikasi iOS, kita perlu untuk mempraktekkan
 langsung teori yang sudah dipelajari
 
 Di bawah ini kalian akan membaca sebuah cerita tentang Pak Asep. Dari cerita tersebut, buatlah pada line di bawahnya sesuai instruksi
 */


// DATA TYPE
/* Buatlah variable dan konstanta dari data sebagai berikut:
 Pak Asep memesan sebuah buah di toko online yang hanya menerima uang dalam USD. 1 USD saat itu ekuivalen dengan Rp. 14500 (konstanta). Pak Asep memesan buah mangga sebanyak 23 buah. Status dari buah itu tersedia (isAvailable)(boolean).
 */

let uang: Int = 14500
var buah: String = "mangga"
var jumlahBuah: Int = 23
var status: Bool = true




// OPERATOR
/*
 Dari konstanta dan variable yang sudah dibuat di atas, buatlah perhitungan sesuai dengan cerita berikut:
 Pak Asep pulang ke rumah membawa buah tersebut. Buah yang sudah dibeli tersebut harus dikurangi sebanyak 5 buah untuk diberikan ke Bu Nita. Lalu sisanya diberikan kepada anaknya yang masih SMA sebanyak 5 buah. Sisanya dimasukkan ke dalam kulkas yang sudah ada 4 buah mangga. Pak Asep harus mencocokkan apakah sudah memberikan buah dengan jumlah yang sama kepada Bu Nita dan anaknya. Berapakah jumlah sisa buah Pak Asep?
 */
var buahPakAsep: Int = jumlahBuah
var buahBuNita: Int = 5
var buahAnaknya: Int = 5
var cekBuah: Bool = buahBuNita == buahAnaknya
var sisaBuah: Int = buahPakAsep - buahBuNita - buahAnaknya
var kulkas: Int = sisaBuah + 4

print("isi buah di dalam kulkas berjumlah \(kulkas)")




// ARRAY
/*
 Buatlah sebuah array yang berisi jumlah buah yang sekarang dimiliki masing-masing dari mereka
 */

var arr: [Int] = [kulkas, buahBuNita, buahAnaknya]
print("buah dalam kulkas ada \(arr[0]), milik Bu anita ada \(arr[1]), dan anaknya ada \(arr[2])")
